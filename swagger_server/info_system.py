import mariadb,datetime
import time
import pymysql
import os,yaml
from typing import Dict, Optional, Set, List, Tuple, Callable
import psycopg2
from jose import JWTError, jwt
JWT_ISSUER = 'com.zalando.connexion'
JWT_SECRET = 'growfarm'
JWT_LIFETIME_SECONDS = 600
JWT_ALGORITHM = 'HS256'
hash_prueba="a5s8f9e6d8f4g1ds2x5d4f7s5e2d"

class InfoSystem:
    def __init__(self):
        with open(r"C:\Users\Aleix\Desktop\bots-python\0.02.0-api\python-flask-server-generated (6)\swagger_server\config.yaml") as f:
            infoSystem_config: Dict = yaml.safe_load(f)
        self.pool_config = infoSystem_config
        self.connection: Optional[pymysql.Connection] = None
        self.connection_payments: Optional[psycopg2.Connection] = None
        print(f"Configurando base de datos")

    def connect_payments_DB(self):
        host=self.pool_config["payouts"]["database"]["host"]
        port=self.pool_config["payouts"]["database"]["port"]
        user=self.pool_config["payouts"]["database"]["user"]
        password=self.pool_config["payouts"]["database"]["password"]
        db=self.pool_config["payouts"]["database"]["db"]

        print(f" PUERTO {port}  ")

        print(f" {host}  {port}   {user}   {password}   {db}  ")
        self.connection_payments = psycopg2.connect(host=host,port=port,user=user,password=password,database=db)
        return self.connection_payments
    
    def connect_pool_DB(self):
        hostM=self.pool_config["mariadb"]["database"]["host"]
        portM=self.pool_config["mariadb"]["database"]["port"]
        userM=self.pool_config["mariadb"]["database"]["user"]
        passwordM=self.pool_config["mariadb"]["database"]["password"]
        dbM=self.pool_config["mariadb"]["database"]["db"]

        print(f" PUERTO {portM}  ")

        print(f" {hostM}  {portM}   {userM}   {passwordM}   {dbM}  ")
        self.connection = pymysql.connect(host=hostM,port=portM,user=userM,password=passwordM,database=dbM)
        return self.connection
    def check_auth(self,rhash):
        try:
            self.connection_payments = self.connect_payments_DB()
            cursor_payments = self.connection_payments.cursor()
            cursor_payments.execute(f"select * from token_validity where hash = '{rhash}'")
            r = cursor_payments.fetchone()
            if len(r) > 0:
                return True
            else:
                return False
        except Exception as e:
            print(e)
            return "hash incorrecto"
        finally:
            self.connection_payments.close()

    def info_pool(self,coin,interval_time):
        self.connection_payments = self.connect_payments_DB()
        cursor_payments = self.connection_payments.cursor()
        self.connection_pool = self.connect_pool_DB()
        cursor_pool = self.connection_pool.cursor()
        ####POOL DB
        print("Succesfully connected")
        cursor_pool.execute(f"SELECT SUM(points) from farmer where is_pool_member = 1")
        points_rows = cursor_pool.fetchone()
        # print(f"Total Points : {points_rows}")
        cursor_pool.execute(f"SELECT MAX(timestamp) from partial where timestamp >= {time.time() - 360}")#buscamos los usuarios que mandaron algun parcial en menos de 6 minutos, lo que serian 12,5 a la hora, 300 al dia. Si no envian un parcial en menos de este tiempo no se uentan como activos
        active_workers = len(cursor_pool.fetchall())
        # print(f"Active workers : {active_workers}")

        ####PAYOUTS DB
        cursor_payments.execute(f"SELECT SUM(amount) from reward r inner join workers w on r.worker = w.id"\
            f" inner join coins on w.coin = coins.id where r.timestamp >= '{datetime.date.today() - datetime.timedelta(days = 1)}' AND coins.generic_mane = '{coin}'")
        amount_24h = cursor_payments.fetchone()
        # print(f"Amount : {amount_24h}")

        cursor_payments.execute(f"SELECT SUM(amount) from reward r inner join workers w on r.worker = w.id"\
            f" inner join coins on w.coin = coins.id where r.timestamp >= '{datetime.date.today() - datetime.timedelta(days = int(interval_time))}' AND coins.generic_mane = '{coin}'")
        amount_n_days = cursor_payments.fetchone()
        # print(f"Amount : {amount_n_days}")

        pool_size = (int(points_rows[0]) / 86400*1.088e-15)/(1024^4)
        # print(f"Amount 24h = {amount_24h}, Pool size = {pool_size},amount N days ={amount_n_days}")
        cursor_payments.execute(f"SELECT number, hash, worker_coin_id from reward r inner join workers w on r.worker = w.id inner join"\
            f" coins on w.coin = coins.id where r.timestamp >= '{datetime.date.today() - datetime.timedelta(days = 1)}' AND coins.generic_mane = '{coin}'")
        blocks_24h = cursor_payments.fetchall()
            
        print(type(blocks_24h[0]),type(blocks_24h[1]),type(blocks_24h[2]))
        cursor_payments.execute(f"SELECT * from reward r inner join workers w on r.worker = w.id inner join"\
                f" coins on w.coin = coins.id where r.timestamp >= '{datetime.date.today() - datetime.timedelta(days = int(interval_time))}' AND coins.generic_mane = '{coin}'")
        blocks_N = cursor_payments.fetchall()
        
        # print(f"Blocks24h = {blocks_24h},  BlocksN = {blocks_N}, active_workers = {active_workers}")
        self.connection_payments.close()
        self.connection_pool.close()

        # return points_rows
        resp = [pool_size, active_workers,  len(blocks_24h),len(blocks_N), points_rows[0],amount_24h[0],amount_n_days[0],blocks_24h]
        print(resp)
        return resp


    def info_worker(self,coin,worker_id):
        is_active=False
        self.connection_payments = self.connect_payments_DB()
        cursor_payments = self.connection_payments.cursor()
        self.connection_pool = self.connect_pool_DB()
        cursor_pool = self.connection_pool.cursor()
        ####POOL DB
        cursor_pool.execute(f"SELECT MAX(timestamp) from partial where launcher_id = '{worker_id}' AND timestamp >= {time.time()-360}")
        if len(cursor_pool.fetchall()) > 0:
            is_active=True 
        else:
            is_active = False
        cursor_payments.execute(f"SELECT COUNT(*) FROM reward r inner join workers on r.worker = workers.id"\
            f" inner join coins on workers.coin = coins.id where workers.worker_coin_id = '{worker_id}' AND coins.generic_mane = '{coin}'")
        blocks_founded = cursor_payments.fetchone()
        cursor_pool.execute(f"SELECT difficulty,points from farmer where launcher_id = '{worker_id}'")
        for r in cursor_pool.fetchall():
            difficulty = r[0]
            points = r[1]
        payment_state = 'payed'

        ####PAYOUTS DB
        cursor_payments.execute(f"SELECT sum(mount) FROM payments INNER JOIN payments_at_workers"\
            f" ON payments.id = payments_at_workers.payment inner join workers w on payments_at_workers.worker = w.id "\
            f" inner join coins c on w.coin = c.id where w.worker_coin_id = '{worker_id}' and c.generic_mane = '{coin}'"
            f" and payments.state = (select s.id from states_of_payments s where s.description = 'payed')")#cambiar per payment_state = payed
        rewards_payed = cursor_payments.fetchone()
        
        ####POOL DB
        cursor_payments.execute(f"SELECT sum(mount) FROM payments INNER JOIN payments_at_workers"\
            f" ON payments.id = payments_at_workers.payment inner join workers w on payments_at_workers.worker = w.id "\
            f" inner join coins c on w.coin = c.id where w.worker_coin_id = '{worker_id}' and c.generic_mane = '{coin}'"
            f" and payments.state = (select s.id from states_of_payments s where s.description = 'await')")#cambiar per payment_state = payed
        rewards_unpayed = cursor_payments.fetchone()
        cursor_pool.execute("SELECT SUM(points) from farmer where is_pool_member = 1")
        total_points = cursor_pool.fetchone()
        cursor_pool.execute("SELECT COUNT(*) from reward")
        coins_mined = cursor_pool.fetchone()
        mojo_per_point = (coins_mined[0]/2) / float(total_points[0])
        estimate_today_reward = mojo_per_point * points
        percentage_participative = points / total_points[0] * 100
        cursor_pool.execute(f"SELECT * from historical_partials where launcher_id = '{worker_id}'")
        valid_partials = cursor_pool.fetchall()
        percentage_valid_partials = len(valid_partials)##100 - ((unvalid_partials / valid_partials)*100)
        farmer_estimate_size = (int(points) / (86400*1.088e-15))/(1024^4)
        response = [points,is_active,blocks_founded[0],rewards_payed,rewards_unpayed[0],percentage_valid_partials,difficulty]
        self.connection_payments.close()
        self.connection_pool.close()
        return response

    def partials(self,worker_id,coin):
        
        self.connection_pool = self.connect_pool_DB()
        cursor_pool = self.connection_pool.cursor()
        ####POOL DB
        cursor_pool.execute(f"SELECT * from historical_partials where launcher_id = '{worker_id}'" )
        p= cursor_pool.fetchall()
        cursor_pool.execute(f"SELECT * from partial where launcher_id = '{worker_id}'" )
        recent_p= cursor_pool.fetchall()
        # print (f"array of Historical partials : {p}")
        # print (f"array of Recent partials : {recent_p}")
        response = recent_p + p
        # print(f"response : {response}")
        self.connection_pool.close()
        return response

    def check_token_DB(self,token):
    #revisamos el token decodificado en la base de datos, si es el mismo en la base de datos y entrado puede porseguir
        self.connection_payments = self.connect_payments_DB()
        cursor_payments = self.connection_payments.cursor()
        decoded = jwt.decode(token, JWT_SECRET, algorithms=[JWT_ALGORITHM])
        print(decoded['sub'])
        cursor_payments.execute(f"SELECT hash from token_validity where hash = '{decoded['sub']}' ")
        t = cursor_payments.fetchall()
        if len(t) > 0:
            self.connection_payments.close()
            return True
        else:
            self.connection_payments.close()
            return False
    def create_user(self, username, password):
        self.connection_payments = self.connect_payments_DB()
        cursor_payments = self.connection_payments.cursor()
        cursor_payments.execute(f"INSERT INTO userdb (username,pass) VALUES ('{username}', '{password}')")
        insertions = cursor_payments.rowcount
        self.connection_payments.commit()
        cursor_payments.execute(f"select max(id) from userdb")
        uid = cursor_payments.fetchone()
        self.connection_payments.close()
        return uid[0]    

    def associate_worker_id(self,uid,worker_id):
        self.connection_payments = self.connect_payments_DB()
        cursor_payments = self.connection_payments.cursor()
        cursor_payments.execute(f"UPDATE userdb SET worker_coin_id = '{worker_id}' WHERE id = {uid}")
        insertions = cursor_payments.rowcount
        self.connection_payments.commit()
        self.connection_payments.close()
        return insertions 

    def delete_user_in_DB(self,username):
        self.connection_payments = self.connect_payments_DB()
        cursor_payments = self.connection_payments.cursor()
        cursor_payments.execute(f"delete from userdb where username = '{username}'")
        self.connection_payments.commit()
        deletions = cursor_payments.rowcount
        self.connection_payments.close()
        return deletions
        

    def delete_worker_in_DB(self, username,worker_id):
        self.connection_payments = self.connect_payments_DB()
        cursor_payments = self.connection_payments.cursor()
        cursor_payments.execute(f"UPDATE userdb SET worker_coin_id = '{None}' where username = '{username}' and worker_coin_id = '{worker_id}'")
        self.connection_payments.commit()
        deleted_worker_coin = cursor_payments.rowcount
        self.connection_payments.close()
  
        return deleted_worker_coin

# def partials(worker_id,coin):
#     ####POOL DB
#     cur,conn = connection("pooldb","connect")
#     cur.execute("SELECT * from historical_partials where worker_id = ? AND coin = ?",(worker_id,coin,))
#     p= cur.fetchall()
#     print (f"array of partials : {p}")
#     connection("pooldb","close",cur,conn)
#     return p





# def associate_worker_id(uid,worker_id):
#     cur,conn = connection("userdb","connect")
#     cur.execute("UPDATE userdb SET worker_id = (?) WHERE uid = (?)",(worker_id,uid,))#RETURNING id1,id2
#     is_created = connection("userdb","commit",cur,conn)
#     connection("userdb","close",cur,conn)
#     return is_created

# def delete_user_in_DB(username):
#     cur,conn = connection("userdb","connect")
#     cur.execute("DELETE FROM userdb where username = (?)",(username,))
#     connection("userdb","commit",cur,conn)
#     connection("userdb","close",cur,conn)

# def delete_worker_in_DB(username,worker_id):
#     cur,conn = connection("userdb","connect")
#     null_value = None
#     cur.execute("UPDATE userdb SET worker_id =(?) where username = (?) and worker_id = (?)",(null_value,username,worker_id,))
#     is_worker_deleted = connection("userdb", "commit",cur,conn)
#     connection("userdb","close",cur,conn)
#     return is_worker_deleted
