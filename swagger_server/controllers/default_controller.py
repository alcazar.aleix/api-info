import connexion
import six
import json
from swagger_server.models.create_associateworker_body import CreateAssociateworkerBody  # noqa: E501
from swagger_server.models.create_body import CreateBody  # noqa: E501
from swagger_server.models.inline_response200 import InlineResponse200  # noqa: E501
from swagger_server.models.inline_response2001 import InlineResponse2001  # noqa: E501
from swagger_server.models.inline_response2002 import InlineResponse2002  # noqa: E501
from swagger_server.models.inline_response400 import InlineResponse400  # noqa: E501
from swagger_server.models.inline_response4001 import InlineResponse4001  # noqa: E501
from swagger_server.models.inline_response4002 import InlineResponse4002  # noqa: E501
from swagger_server.models.inline_response404 import InlineResponse404  # noqa: E501
from swagger_server.models.inline_response200_array_of_blocks import InlineResponse200ArrayOfBlocks
from swagger_server import util
import time
from typing import List
from werkzeug.exceptions import Unauthorized
from jose import JWTError, jwt
import six
import swagger_server.db_config as db
import swagger_server.info_system as InfoSystem
JWT_ISSUER = 'com.zalando.connexion'
JWT_SECRET = 'growfarm'
JWT_LIFETIME_SECONDS = 600
JWT_ALGORITHM = 'HS256'
info_system = InfoSystem.InfoSystem()
def call_global(coin, ndays):  # noqa: E501
    try:   
        rows = info_system.info_pool(coin,ndays)
        for row in rows:
            response = row
        try:
            return InlineResponse200(rows[0],rows[1],rows[2],rows[3],rows[4],rows[5],rows[6],InlineResponse200ArrayOfBlocks(rows[7]))
        except Exception as e:
            return InlineResponse4001("Invalid launcher id")    
    except Exception as e:
        print(e)
        #return jsonify(e)
    return 'error'

def create_associate_worker_put(body):  # noqa: E501
    print(body['uid'])
    print(body['worker-id'])
    is_valid = info_system.associate_worker_id(body['uid'],body['worker-id'])
    print(is_valid)
    if connexion.request.is_json:
        body = CreateAssociateworkerBody.from_dict(connexion.request.get_json())  # noqa: E501
    return body


def create_post(body):  # noqa: E501
    is_valid = info_system.create_user(body['username'], body['password'])
    print(body)
    if connexion.request.is_json:
        body = CreateBody.from_dict(connexion.request.get_json())  # noqa: E501
    print(is_valid)
        
    return is_valid


def delete_user(username):  # noqa: E501
    users_deleted = info_system.delete_user_in_DB(username)
    return users_deleted


def delete_worker(username, workerid):  # noqa: E501
    is_workerid_deleted = info_system.delete_worker_in_DB(username, workerid)
    return is_workerid_deleted


def generate_token(rhash):  # noqa: E501    
    is_valid = False
    is_valid = info_system.check_auth(rhash)
    if is_valid == True:
        timestamp = time.time()
        payload = {
            "iss": JWT_ISSUER,
            "iat": int(timestamp),
            "exp": int(timestamp + JWT_LIFETIME_SECONDS),
            "sub": str(rhash),
        }
        print(payload)
        print(jwt.encode(payload, JWT_SECRET, algorithm=JWT_ALGORITHM))
        return jwt.encode(payload, JWT_SECRET, algorithm=JWT_ALGORITHM)
    else:
        print("token no valido")
        return "Token no valido"
    # db.add_token_in_DB(jwt.encode(payload, JWT_SECRET, algorithm=JWT_ALGORITHM))
    #### INLCUIR TOKEN ENCODE EN LA BASE DE DATOS ####
    


def partials(coin, workerid): # noqa: E# noqa: E501
    try:   
        rows = info_system.partials(workerid, coin)
        for row in rows:
            response = row
        try:
            return InlineResponse2002(rows)
        except Exception as e:
            return InlineResponse4001("Invalid launcher id")    
    except Exception as e:
        print(e)

    return 'partials!'


def worker_id(coin, workerid):  # noqa: E501
    try:   
        rows = info_system.info_worker(coin,workerid)
        try:
            return InlineResponse2001(rows[0],rows[1],rows[2],rows[3],rows[4],rows[5],rows[6])
        except Exception as e:
            print (e)
            return e
    except Exception as ex:
        print(ex)
        return ex

