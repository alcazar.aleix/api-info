from typing import List
from werkzeug.exceptions import Unauthorized
from jose import JWTError, jwt
import six
from swagger_server.info_system import InfoSystem
JWT_ISSUER = 'com.zalando.connexion'
JWT_SECRET = 'growfarm'
JWT_LIFETIME_SECONDS = 600
JWT_ALGORITHM = 'HS256'
hash_prueba="a5s8f9e6d8f4g1ds2x5d4f7s5e2d"
"""
controller generated to handled auth operation described at:
https://connexion.readthedocs.io/en/latest/security.html
"""
infoSystem = InfoSystem()
def check_token_in_DB(token):
    is_valid = False
    ## Chequea en la BBDD si el decoded_token['sub'] ( el hash generado) es el mismo que hemos generado en chia ademas de validar que sea un jwt valido
    is_valid = infoSystem.check_token_DB(token)
    return is_valid

def check_jwt(token):
    decoded_token = decode_token(token)
    #print(decoded_token['sub'])
    ## revisar en base de datos y confirmar que se ha generado en cliente de chia y es el 
    if check_token_in_DB(token) == True:
        print(token)
        print(decoded_token['sub'])
        return {'test_key': 'test_value'}
    # Retornar el token codificado y que se copie en el header de la peticion request  create
    else:
        print("unauthorized")
        # print(decoded_token)
        raise  Unauthorized


def decode_token(token):
    try:
        return jwt.decode(token, JWT_SECRET, algorithms=[JWT_ALGORITHM])
    except JWTError as e:
        raise Unauthorized from e
    except JWTError as e:
        six.raise_from(Unauthorized, e)