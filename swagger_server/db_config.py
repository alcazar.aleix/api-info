import mariadb
import time
import pymysql
import os,yaml
from typing import Dict, Optional, Set, List, Tuple, Callable
import psycopg2
from jose import JWTError, jwt
JWT_ISSUER = 'com.zalando.connexion'
JWT_SECRET = 'growfarm'
JWT_LIFETIME_SECONDS = 600
JWT_ALGORITHM = 'HS256'
hash_prueba="a5s8f9e6d8f4g1ds2x5d4f7s5e2d"

class InfoSystem:
    def __init__(self):
        with open(os.getcwd() + "/config.yaml") as f:
            infoSystem_config: Dict = yaml.safe_load(f)
        self.pool_config = infoSystem_config
        self.connection: Optional[pymysql.Connection] = None
        self.connection_payments: Optional[psycopg2.Connection] = None
        print(f"Configurando base de datos")

    def connect_payments_DB(self):
        host=self.pool_config["payouts"]["database"]["host"]
        port=self.pool_config["payouts"]["database"]["port"]
        user=self.pool_config["payouts"]["database"]["user"]
        password=self.pool_config["payouts"]["database"]["password"]
        db=self.pool_config["payouts"]["database"]["db"]

        print(f" PUERTO {port}  ")

        print(f" {host}  {port}   {user}   {password}   {db}  ")
        self.connection_payments = psycopg2.connect(host=host,port=port,user=user,password=password,database=db)
        return self.connection_payments
    
    def connect_pool_DB(self):
        hostM=self.pool_config["mariadb"]["database"]["host"]
        portM=self.pool_config["mariadb"]["database"]["port"]
        userM=self.pool_config["mariadb"]["database"]["user"]
        passwordM=self.pool_config["mariadb"]["database"]["password"]
        dbM=self.pool_config["mariadb"]["database"]["db"]

        print(f" PUERTO {portM}  ")

        print(f" {hostM}  {portM}   {userM}   {passwordM}   {dbM}  ")
        self.connection = pymysql.connect(host=hostM,port=portM,user=userM,password=passwordM,database=dbM)
        return self.connection
def connection(db_name,action,curs = None,conn = None):
    if action == 'connect':
        try:
            conn = mariadb.connect(
                user = "user",
                password = "growfarm",
                host = "192.168.1.74",
                port = 3306,
                database = db_name
                )
            curs = conn.cursor()
            print("Connection Opened")
            return curs, conn
        except Exception as e:
            print(f"Connection Failed = {e}")
            return e
        return curs
    elif action == 'commit':
        try:
            conn.commit()
            return("user created/updated/deleted")
        except mariadb.Error as error_registro:
            return error_registro
    elif action == 'close':
        try:
            curs.close()
            conn.close()
            return "connection closed"
        except mariadb.Error as error_reg:
            print(error_reg)
            return error_reg

def info_pool(coin,interval_time):
    timestamp = interval_time*24*60*60

    ####POOL DB
    cur,conn = connection("pooldb","connect")
    cur.execute("SELECT SUM(points) from workers where is_pool_member = 1 AND coin = ?",(coin,) )
    points_rows = cur.fetchall()
    print(f"Total Points : {points_rows}")
    connection("pooldb","close",cur,conn)
    return "prova be"
    ####PAYOUTS DB
    cur,conn = connection("paydb","connect")
    cur.execute("SELECT SUM(accumulated_amount) from closures where timestamp >= ? AND coin = ?",(time.time - 86.400,coin,))
    amount_24h = cur.fetchall()
    print(amount_24h)
    cur.execute("SELECT SUM(accumulated_amount) from closures where timestamp >= ? AND coin = ?",(timestamp,coin,))
    amount_n_days = cur.fetchall()
    print(amount_n_days)
    pool_size = (points_rows / (86400*1.088e-15))/1024^4
    print(f"Amount 24h = {amount_24h}, Pool size = {pool_size},amount N days ={amount_n_days}")
    connection("paydb","close",cur,conn)
    
    ####POOL DB
    cur,conn = connection("pooldb","connect")
    cur.execute("SELECT * from block_rewarded where timestamp >= ? AND coin = ?)",(time.time - 86.400,coin,))
    blocks_24h = cur.fetchall()
    cur.execute("SELECT * from block_rewarded where timestamp >= ? AND coin = ?)",(timestamp,coin,))
    blocks_N = cur.fetchall()
    cur.execute("SELECT MAX(timestamp) from partial where timestamp >= ? AND coin = ?",(time.time - 360,coin,))#buscamos los usuarios que mandaron algun parcial en menos de 6 minutos, lo que serian 12,5 a la hora, 300 al dia. Si no envian un parcial en menos de este tiempo no se uentan como activos
    active_workers = len(cur.fetchall())
    print(f"Blocks24h = {blocks_24h},  BlocksN = {blocks_N}, active_workers = {active_workers}")
    connection("pooldb","close",cur,conn)
    # return points_rows
    resp = [blocks_24h,active_workers, points_rows, len(blocks_24h),len(blocks_N),pool_size,amount_24h,amount_n_days,points_rows]
    return resp
def info_worker(coin,worker_id):
    is_active=False

    ####POOL DB
    cur,conn = connection("pooldb","connect")
    cur.execute("SELECT MAX(timestamp) from partial where launcher_id = ? AND timestamp >= ? AND coin = ?",(worker_id,time.time-360,coin,))
    if len(cur.fetchall()) > 0:
         is_active=True 
    else:
        is_active = False
    cur.execute("SELECT * FROM block_rewarded where worker_id = ? AND coin = ?",(worker_id,coin,))
    blocks_founded = cur.fetchall()
    print(blocks_founded)
    cur.execute("SELECT difficulty,points from farmer where worker_id = ? AND coin = ?",(worker_id,coin,))
    for r in cur.fetchall():
        difficulty = r[0]
        points = r[1]
    print(f"Points = {points}, difficulty = {difficulty}, Is Active = {is_active}")
    payment_state = "payed"
    connection("pooldb","close",cur,conn)

    ####PAYOUTS DB
    cur,conn = connection("paydb","connect")
    cur.execute(f"""SELECT mount,datetime_create FROM payments INNER JOIN payments_at_workers
        ON payments.id = payments_at_workers.payment WHERE (SELECT worker_coin_id FROM workers INNER JOIN
        payments_at_workers ON workers.id=payments_at_workers.worker) = {worker_id} AND
        (SELECT generic_name from coins inner join workers on coins.id = workers.coin) = {coin} AND
        (SELECT description from historical_states_of_payments inner join payments on historical_states_of_payments.payment = payments.id) = {payment_state}
        """)
    rewards_payed = cur.fetchall()
    print(f"rewards_payed : {rewards_payed}")
    connection("paydb","close",cur,conn)

    ####POOL DB
    cur,conn = connection("pooldb","connect")
    cur.execute("SELECT SUM(points) from farmer WHERE coin = ? AND is_pool_member = 1",(coin,))
    total_points = cur.fetchone()
    cur.execute("SELECT * from block_rewarded WHERE coin = ?",(coin,))
    coins_mined = (len(cur.fetchall()))*2 
    mojo_per_point = coins_mined / total_points
    estimate_today_reward = mojo_per_point * points
    percentage_participative = points / total_points * 100
    print (f"Total: {total_points}, Worker : {points}, Mined Coins: {coins_mined}, Mojo x point : {mojo_per_point}, Today reward estimate : {estimate_today_reward}, Percentage Participation : {percentage_participative}")
    cur.execute("SELECT * from historical_partials where launcher_id = ? AND valid = 1",(worker_id))
    valid_partials = cur.fetchall()
    cur.execute("SELECT * from historical_partials where launcher_id = ? AND valid = 0",(worker_id))
    unvalid_partials = cur.fetchall()
    percentage_valid_partials = 100 - ((unvalid_partials / valid_partials)*100)
    farmer_estimate_size = (points / (86400*1.088e-15))/1024^4
    print(f"valid partials: {partials}, Farmer Estimated size = {farmer_estimate_size}")
    connection("pooldb","close",cur,conn)
    response = [is_active,[blocks_founded],difficulty,points,rewards_payed,estimate_today_reward,percentage_valid_partials]
    return response
def partials(worker_id,coin):
    ####POOL DB
    cur,conn = connection("pooldb","connect")
    cur.execute("SELECT * from historical_partials where worker_id = ? AND coin = ?",(worker_id,coin,))
    p= cur.fetchall()
    print (f"array of partials : {p}")
    connection("pooldb","close",cur,conn)
    return p

def check_token_DB(token):
    #revisamos el token decodificado en la base de datos, si es el mismo en la base de datos y entrado puede porseguir
    decoded = jwt.decode(token, JWT_SECRET, algorithms=[JWT_ALGORITHM])
    cur,conn = connection("pooldb","connect")
    cur.execute("SELECT token from tokens where token = ? ",(decoded['sub'],))
    t = cur.fetchall()
    if len(t) > 0:
        connection("pooldb","close",cur,conn)
        return True
    else:
        connection("pooldb","close",cur,conn)
        return False

def create_user(uid, username, password):
    cur,conn = connection("userdb","connect")    
    cur.execute("INSERT INTO userdb (uid, username) VALUES (?, ?, ?)",(uid,username, password,))#RETURNING id1,id2
    is_created = connection("userdb","commit",cur,conn)
    connection("userdb","close",cur,conn)
    return is_created

def associate_worker_id(uid,worker_id):
    cur,conn = connection("userdb","connect")
    cur.execute("UPDATE userdb SET worker_id = (?) WHERE uid = (?)",(worker_id,uid,))#RETURNING id1,id2
    is_created = connection("userdb","commit",cur,conn)
    connection("userdb","close",cur,conn)
    return is_created

def delete_user_in_DB(username):
    cur,conn = connection("userdb","connect")
    cur.execute("DELETE FROM userdb where username = (?)",(username,))
    connection("userdb","commit",cur,conn)
    connection("userdb","close",cur,conn)

def delete_worker_in_DB(username,worker_id):
    cur,conn = connection("userdb","connect")
    null_value = None
    cur.execute("UPDATE userdb SET worker_id =(?) where username = (?) and worker_id = (?)",(null_value,username,worker_id,))
    is_worker_deleted = connection("userdb", "commit",cur,conn)
    connection("userdb","close",cur,conn)
    return is_worker_deleted
