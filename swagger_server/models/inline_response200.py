# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from swagger_server.models.base_model_ import Model
from swagger_server.models.inline_response200_array_of_blocks import InlineResponse200ArrayOfBlocks  # noqa: F401,E501
from swagger_server import util


class InlineResponse200(Model):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    def __init__(self, pool_size: float=None, active_workers: float=None, blocks_24h: float=None, blocks_n_days: float=None, total_points: float=None, rewards_last_24h: float=None, rewards_last_n_days: float=None, array_of_blocks: List[InlineResponse200ArrayOfBlocks]=None):  # noqa: E501
        """InlineResponse200 - a model defined in Swagger

        :param pool_size: The pool_size of this InlineResponse200.  # noqa: E501
        :type pool_size: float
        :param active_workers: The active_workers of this InlineResponse200.  # noqa: E501
        :type active_workers: float
        :param blocks_24h: The blocks_24h of this InlineResponse200.  # noqa: E501
        :type blocks_24h: float
        :param blocks_n_days: The blocks_n_days of this InlineResponse200.  # noqa: E501
        :type blocks_n_days: float
        :param total_points: The total_points of this InlineResponse200.  # noqa: E501
        :type total_points: float
        :param rewards_last_24h: The rewards_last_24h of this InlineResponse200.  # noqa: E501
        :type rewards_last_24h: float
        :param rewards_last_n_days: The rewards_last_n_days of this InlineResponse200.  # noqa: E501
        :type rewards_last_n_days: float
        :param array_of_blocks: The array_of_blocks of this InlineResponse200.  # noqa: E501
        :type array_of_blocks: List[InlineResponse200ArrayOfBlocks]
        """
        self.swagger_types = {
            'pool_size': float,
            'active_workers': float,
            'blocks_24h': float,
            'blocks_n_days': float,
            'total_points': float,
            'rewards_last_24h': float,
            'rewards_last_n_days': float,
            'array_of_blocks': List[InlineResponse200ArrayOfBlocks]
        }

        self.attribute_map = {
            'pool_size': 'pool-size',
            'active_workers': 'active-workers',
            'blocks_24h': 'blocks-24h',
            'blocks_n_days': 'blocks-N-days',
            'total_points': 'total-points',
            'rewards_last_24h': 'rewards-last-24h',
            'rewards_last_n_days': 'rewards-last-N-days',
            'array_of_blocks': 'ArrayOfBlocks'
        }
        self._pool_size = pool_size
        self._active_workers = active_workers
        self._blocks_24h = blocks_24h
        self._blocks_n_days = blocks_n_days
        self._total_points = total_points
        self._rewards_last_24h = rewards_last_24h
        self._rewards_last_n_days = rewards_last_n_days
        self._array_of_blocks = array_of_blocks

    @classmethod
    def from_dict(cls, dikt) -> 'InlineResponse200':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The inline_response_200 of this InlineResponse200.  # noqa: E501
        :rtype: InlineResponse200
        """
        return util.deserialize_model(dikt, cls)

    @property
    def pool_size(self) -> float:
        """Gets the pool_size of this InlineResponse200.


        :return: The pool_size of this InlineResponse200.
        :rtype: float
        """
        return self._pool_size

    @pool_size.setter
    def pool_size(self, pool_size: float):
        """Sets the pool_size of this InlineResponse200.


        :param pool_size: The pool_size of this InlineResponse200.
        :type pool_size: float
        """

        self._pool_size = pool_size

    @property
    def active_workers(self) -> float:
        """Gets the active_workers of this InlineResponse200.


        :return: The active_workers of this InlineResponse200.
        :rtype: float
        """
        return self._active_workers

    @active_workers.setter
    def active_workers(self, active_workers: float):
        """Sets the active_workers of this InlineResponse200.


        :param active_workers: The active_workers of this InlineResponse200.
        :type active_workers: float
        """

        self._active_workers = active_workers

    @property
    def blocks_24h(self) -> float:
        """Gets the blocks_24h of this InlineResponse200.


        :return: The blocks_24h of this InlineResponse200.
        :rtype: float
        """
        return self._blocks_24h

    @blocks_24h.setter
    def blocks_24h(self, blocks_24h: float):
        """Sets the blocks_24h of this InlineResponse200.


        :param blocks_24h: The blocks_24h of this InlineResponse200.
        :type blocks_24h: float
        """

        self._blocks_24h = blocks_24h

    @property
    def blocks_n_days(self) -> float:
        """Gets the blocks_n_days of this InlineResponse200.


        :return: The blocks_n_days of this InlineResponse200.
        :rtype: float
        """
        return self._blocks_n_days

    @blocks_n_days.setter
    def blocks_n_days(self, blocks_n_days: float):
        """Sets the blocks_n_days of this InlineResponse200.


        :param blocks_n_days: The blocks_n_days of this InlineResponse200.
        :type blocks_n_days: float
        """

        self._blocks_n_days = blocks_n_days

    @property
    def total_points(self) -> float:
        """Gets the total_points of this InlineResponse200.


        :return: The total_points of this InlineResponse200.
        :rtype: float
        """
        return self._total_points

    @total_points.setter
    def total_points(self, total_points: float):
        """Sets the total_points of this InlineResponse200.


        :param total_points: The total_points of this InlineResponse200.
        :type total_points: float
        """

        self._total_points = total_points

    @property
    def rewards_last_24h(self) -> float:
        """Gets the rewards_last_24h of this InlineResponse200.


        :return: The rewards_last_24h of this InlineResponse200.
        :rtype: float
        """
        return self._rewards_last_24h

    @rewards_last_24h.setter
    def rewards_last_24h(self, rewards_last_24h: float):
        """Sets the rewards_last_24h of this InlineResponse200.


        :param rewards_last_24h: The rewards_last_24h of this InlineResponse200.
        :type rewards_last_24h: float
        """

        self._rewards_last_24h = rewards_last_24h

    @property
    def rewards_last_n_days(self) -> float:
        """Gets the rewards_last_n_days of this InlineResponse200.


        :return: The rewards_last_n_days of this InlineResponse200.
        :rtype: float
        """
        return self._rewards_last_n_days

    @rewards_last_n_days.setter
    def rewards_last_n_days(self, rewards_last_n_days: float):
        """Sets the rewards_last_n_days of this InlineResponse200.


        :param rewards_last_n_days: The rewards_last_n_days of this InlineResponse200.
        :type rewards_last_n_days: float
        """

        self._rewards_last_n_days = rewards_last_n_days

    @property
    def array_of_blocks(self) -> List[InlineResponse200ArrayOfBlocks]:
        """Gets the array_of_blocks of this InlineResponse200.


        :return: The array_of_blocks of this InlineResponse200.
        :rtype: List[InlineResponse200ArrayOfBlocks]
        """
        return self._array_of_blocks

    @array_of_blocks.setter
    def array_of_blocks(self, array_of_blocks: List[InlineResponse200ArrayOfBlocks]):
        """Sets the array_of_blocks of this InlineResponse200.


        :param array_of_blocks: The array_of_blocks of this InlineResponse200.
        :type array_of_blocks: List[InlineResponse200ArrayOfBlocks]
        """

        self._array_of_blocks = array_of_blocks
