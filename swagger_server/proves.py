import mariadb,datetime
import time
import pymysql,hashlib,math
import os,yaml
from typing import Dict, Optional, Set, List, Tuple, Callable
import psycopg2
from jose import JWTError, jwt
JWT_ISSUER = 'com.zalando.connexion'
JWT_SECRET = 'growfarm'
JWT_LIFETIME_SECONDS = 600
JWT_ALGORITHM = 'HS256'
hash_prueba="a5s8f9e6d8f4g1ds2x5d4f7s5e2d"
class InfoSystem:
    def __init__(self):
        with open(r"C:\Users\Aleix\Desktop\bots-python\0.02.0-api\python-flask-server-generated (6)\swagger_server\config.yaml") as f:
            infoSystem_config: Dict = yaml.safe_load(f)
        self.pool_config = infoSystem_config
        self.connection: Optional[pymysql.Connection] = None
        self.connection_payments: Optional[psycopg2.Connection] = None
        print(f"Configurando base de datos")

    def connect_payments_DB(self):
        host=self.pool_config["payouts"]["database"]["host"]
        port=self.pool_config["payouts"]["database"]["port"]
        user=self.pool_config["payouts"]["database"]["user"]
        password=self.pool_config["payouts"]["database"]["password"]
        db=self.pool_config["payouts"]["database"]["db"]

        print(f" PUERTO {port}  ")

        print(f" {host}  {port}   {user}   {password}   {db}  ")
        self.connection_payments = psycopg2.connect(host=host,port=port,user=user,password=password,database=db)
        return self.connection_payments
    
    def connect_pool_DB(self):
        hostM=self.pool_config["mariadb"]["database"]["host"]
        portM=self.pool_config["mariadb"]["database"]["port"]
        userM=self.pool_config["mariadb"]["database"]["user"]
        passwordM=self.pool_config["mariadb"]["database"]["password"]
        dbM=self.pool_config["mariadb"]["database"]["db"]

        print(f" PUERTO {portM}  ")

        print(f" {hostM}  {portM}   {userM}   {passwordM}   {dbM}  ")
        self.connection = pymysql.connect(host=hostM,port=portM,user=userM,password=passwordM,database=dbM)
        return self.connection
    def prova(self):
        self.connection_payments = self.connect_payments_DB()
        cursor_payments = self.connection_payments.cursor()
        self.connection_pool = self.connect_pool_DB()
        cursor_pool = self.connection_pool.cursor()
        segundos, timestamp = math.modf(datetime.datetime.timestamp(datetime.datetime.utcnow()))
        hash = hashlib.md5()
        hash.update(str(timestamp).encode())
        hash = hash.hexdigest()
        cursor_payments.execute(f"INSERT INTO token_validity (hash) VALUES('{hash}')")
        self.connection_payments.commit()

# prova = InfoSystem()
# prova.prova()
print((34557035 / (86400*1.088e-15))/(1024^4))