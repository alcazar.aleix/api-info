# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.create_associateworker_body import CreateAssociateworkerBody  # noqa: E501
from swagger_server.models.create_body import CreateBody  # noqa: E501
from swagger_server.models.inline_response200 import InlineResponse200  # noqa: E501
from swagger_server.models.inline_response2001 import InlineResponse2001  # noqa: E501
from swagger_server.models.inline_response2002 import InlineResponse2002  # noqa: E501
from swagger_server.models.inline_response400 import InlineResponse400  # noqa: E501
from swagger_server.models.inline_response4001 import InlineResponse4001  # noqa: E501
from swagger_server.models.inline_response4002 import InlineResponse4002  # noqa: E501
from swagger_server.models.inline_response404 import InlineResponse404  # noqa: E501
from swagger_server.test import BaseTestCase


class TestDefaultController(BaseTestCase):
    """DefaultController integration test stubs"""

    def test_call_global(self):
        """Test case for call_global

        Read the global information of Growfarm pool in a selected coin
        """
        response = self.client.open(
            '/v1//{coin}/global/{ndays}'.format(coin='coin_example', ndays=1.2),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_create_associate_worker_put(self):
        """Test case for create_associate_worker_put

        
        """
        body = CreateAssociateworkerBody()
        response = self.client.open(
            '/v1//create/associate-worker',
            method='PUT',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_create_post(self):
        """Test case for create_post

        
        """
        body = CreateBody()
        response = self.client.open(
            '/v1//create',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_delete_user(self):
        """Test case for delete_user

        
        """
        response = self.client.open(
            '/v1//delete/{username}'.format(username='username_example'),
            method='DELETE')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_delete_worker(self):
        """Test case for delete_worker

        
        """
        response = self.client.open(
            '/v1//delete/{username}/{workerid}'.format(username='username_example', workerid='workerid_example'),
            method='DELETE')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_generate_token(self):
        """Test case for generate_token

        Return JWT token
        """
        response = self.client.open(
            '/v1//auth/{rhash}'.format(rhash='rhash_example'),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_partials(self):
        """Test case for partials

        Read the partials/hashes of worker in a Growfarm Pool
        """
        response = self.client.open(
            '/v1//{coin}/{workerid}/partials'.format(workerid='workerid_example', coin='coin_example'),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_worker_id(self):
        """Test case for worker_id

        Read the information of worker in a Growfarm Pool
        """
        response = self.client.open(
            '/v1//{coin}/{workerid}'.format(workerid='workerid_example', coin='coin_example'),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
